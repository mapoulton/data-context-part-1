package uk.gov.stockport.testing.datacontextexample.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataContext extends SQLiteOpenHelper {
	public final static String DATABASE_NAME = "TestingDatabase";
	public final static int DATABASE_VERSION = 1; 
	
	public DataContext(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public static final class TBL_PEOPLE {
		public static final String DATABASE_CREATE = "create table "
		      + TBL_PEOPLE.TABLE_NAME + " ( " + TBL_PEOPLE.KEY_ID + " "
		      + "integer primary key autoincrement, "
		      + TBL_PEOPLE.KEY_NAME + " text not null);";
		
		public static final String DATABASE_UPDATE_1_TO_2 = "ALTER TABLE " 
				+ TBL_PEOPLE.TABLE_NAME + " ADD COLUMN " + TBL_PEOPLE.KEY_AGE 
				+ " integer";
		
		public static final String DATABASE_UPDATE_2_TO_3 = "";
		public static final String DATABASE_UPDATE_3_TO_4 = "";
		
		public static String TABLE_NAME = "People";
		public static String KEY_ID = "_id";
		public static String KEY_NAME = "person";
		public static String KEY_AGE = "age";
	}

	@Override
	public void onCreate(SQLiteDatabase database) { 
		database.execSQL(TBL_PEOPLE.DATABASE_CREATE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase database, int versionFrom, int versionTo) {
		switch(versionFrom){
			case 1:
				database.execSQL(TBL_PEOPLE.DATABASE_UPDATE_1_TO_2);
			case 2:
				database.execSQL(TBL_PEOPLE.DATABASE_UPDATE_2_TO_3);
			case 3:
				database.execSQL(TBL_PEOPLE.DATABASE_UPDATE_3_TO_4);
		}
	}
	
	private SQLiteDatabase pDatabase;
	public void open(){
		if(pDatabase == null)
			pDatabase = this.getWritableDatabase();
	}
	
	public void close() { 
		if(pDatabase != null)
			pDatabase.close();
	}
	
	public Cursor Person(int id){
		String[] allCols = new String[] { TBL_PEOPLE.KEY_ID,
				TBL_PEOPLE.KEY_NAME,
				TBL_PEOPLE.KEY_AGE};
		String whereClause = TBL_PEOPLE.KEY_ID + "=?"; 
		String[] arguments = new String[] { Integer.toString(id) };
	
		return pDatabase.query(TBL_PEOPLE.TABLE_NAME, allCols,
				whereClause, arguments, 
				null, null, null, null);
	}
	
	public Cursor People(){
		return pDatabase.query(TBL_PEOPLE.TABLE_NAME,
				new String[] {TBL_PEOPLE.KEY_ID,
				TBL_PEOPLE.KEY_NAME,
				TBL_PEOPLE.KEY_AGE},
					null, null, null, null, null);
	}

}
